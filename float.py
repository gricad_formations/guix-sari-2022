import numpy as np
from numpy.random import randn as rand

M = 1024
N = 2048
np.random.seed(0)

a = rand(M,N).astype(dtype=np.float32)
w = rand(N,M).astype(dtype=np.float32)

b = np.dot(a, w)

for i in range(10):
    b += np.dot(b, a)[:, :1024]
    b /= 100.

print("Three first final matrix elements, on the first line:")
print(b[0,:3])
