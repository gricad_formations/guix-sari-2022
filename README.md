# Guix SARI 2022

## Résumé

SARI et Pierre-Antoine (Bouttier) de l'UAR GRICAD vous proposent un atelier de prise en main de GUIX.
GUIX est un gestionnaire de paquets et permet d'assurer une reproductibilité des environnements logiciels.
Il est de plus en plus utilisé dans les environnements HPC pour assurer de la reproductibilité mais pas que..

## Liens vers les slides 

* [Version HTML...](https://gricad_formations.gricad-pages.univ-grenoble-alpes.fr/guix-sari-2022/sari_guix_2022.html)
* [...OU PDF](https://gricad_formations.gricad-pages.univ-grenoble-alpes.fr/guix-sari-2022/sari_guix_2022.pdf)