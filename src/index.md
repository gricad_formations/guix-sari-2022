---
marp: true
theme: gricad
footer: "UAR GRICAD - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---
 
# GRICAD@SARI 2022 - Slides

[GUIX@SARI - 08/12/2022](./sari_guix_2022.html) - [pdf](./sari_guix_2022.pdf)

