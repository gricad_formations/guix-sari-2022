---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "GUIX@SARI, 08/12/2022- *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Guix, un gestionnaire d'environnement logiciel pour la reproductibilité

*Guix@SARI, 08/12/2022*
*pierre-antoine.bouttier@univ-grenoble-alpes.fr*


---

# Introduction

--- 
## D'où je parle

* IR CNRS, expert en calcul scientifique 
* Pas ASR...
* ...et encore moins chercheur en informatique :)
* Responsable, à GRICAD, de l'équipe CS:Données (accompagnement des utilisateurs effectifs ou potentiels de nos services numériques pour le calcul scientifique et la gestion des données de la recherche)

---
## Un petit mot sur la reproductibilité

* Reproductibilité expérimentale
  - Refaire une expérience d'après la description publiée
  - Obtenir des résultats suffisamment proches
* Reproductibilité statistique
  - Refaire une étude avec un autre échantillon ou une autre technique
  - Inférer des conclusions  suffisamment proches
* Reproductibilité computationnelle
  - Refaire un calcul à l'identique
  - Obtenir des résultats à l'identique

--- 
# Expérimentation numérique

![w:900 center](./fig/num.png)

---
## Le reproductibilité computationnelle

- Reproductibilité expérimentale
  - Refaire une expérience d'après la description publiée
  - Obtenir des résultats suffisamment proches
- Reproductibilité statistique
  - Refaire une étude avec un autre échantillon ou une autre technique
  - Inférer des conclusions  suffisamment proches
- **Reproductibilité computationnelle**
  - Refaire un calcul à l'identique
  - Obtenir des résultats à l'identique

* Dans le cadre numérique, nous devons **tendre**, a minima, vers le reproductibilité computationnelle. 

--- 
# Expérimentation(s) numérique(s)

![w:900 center](./fig/num3.png)

---
## La problématique des environnements logiciels

![h:600 center](./fig/xkcd_dep.png)

---
# L'utopie logicielle pour le HPC

* **Maintenance** (arbre des dépendances bien géré, OS-indépendant), **reproductibilité** (version unique d'un paquet - src, compilation, desc, déf,...- a la même sortie où que ce soit), **portabilité**
* **Gestion avancée des permissions** (environnement utilisateur, paquets/environnements spécifiques à une communauté, licences)
* **Worklow automatisé** : paquets *custom*, rebuilds automatiques, du PC perso aux clusters, CI
* Options de compilation avancée
* **OS-indépendant !**  

---
# Welcome to the jungle (out-of-date?)

![w:1000 center](fig/jungle_softenv.png)

---
# Pourquoi Guix ? 

* **Fait pour la reproductibité logicielle, par construction**
  * Comme Nix
  * Au contraire de Spack, conda, modules, très dépendant du système hôte
* Pour le HPC : 
  * Pas besoin des droits administrateur pour mettre en place n'importe quel environnement logiciel
  * **Communauté** française active
  * **Reproductibilité** implique **portabilité**
  * AMHA, interface CLI et langage relativement plus abordable que ceux de Nix

---
# Pour aller plus loin 

Quelques articles/présentations qui explicitent les points ci-avant :  
- [Reproduire les environnements logiciels : un maillon incontournable de la recherche reproductible, *L. Courtès*](https://www.societe-informatique-de-france.fr/wp-content/uploads/2021/11/1024_18_2021_15.html)
- [3 présentations à propos des gestionnaires d'env. log. reproductibles](https://reproducibility.gricad-pages.univ-grenoble-alpes.fr/web/medias_251121.html#medias_251121)

---

# Les mains dans le cambouix

---
# Deux termes et définitions basiques

* Un **paquet** GUIX : Une **définition** (*=code source, fichier texte brut*) de l'ensemble des instructions et dépendances pour installer un logiciel
* Un **channel** Guix : un **dépôt git contenant un ensemble de définitions** de paquets (et quelques fichiers de configurations). Un numéro de commit particulier (donc **un état bien identifié des définitions**) de ce dépôt peut être appelé **révision**. Un commit de ceux dépôt peut être vu comme un instantané de l'arbre de dépendances complet.

---
### Principaux mécanismes de GUIX

* Chaque paquet est unique et identifié. Une fois construits à partir du fichier de définitions, les paquets sont tous stockés sous le même répertoire `/gnu/store`
* Quand l'utilisateur installe un paquet, `guix` le construit puis créée **des liens symboliques** dans un répertoire (= profil) appartenant à l'utilisateur (par défaut, `$HOME/.guix-profile/`) 
* Ensuite, pilotage par les variables d'env (*e.g.* `PATH`, `PYTHONPATH`...)
* A chaque modification de la définition du paquet, si on souhaite le reconstruire, un nouveau paquet sera créée (**unicité des paquets**)
* Dans le cadre de logiciels open-source, l'ensemble de l'arbre de dépendance est construit à partir des sources.

--- 
# La reproductibilité de l'environnement logiciel avec GUIX

Au-delà des mécanismes internes de GUIX pour assurer la reproductibilité logicielle, en tant qu'utilisateur, **puisque les paquets que vous avez installés (et toutes leurs dépendances) correspondent à un état des définitions bien identifiées** (par le numéro de commit du channel sur lequel vous pointez au moment d'installer le paquet), **il est facile, sur une autre machine et à un autre moment, de réinstaller ce même environnement en indiquant cet état des définitions** (=n° de commit du channel).

---
# Passons à la pratique

Prérequis : 
- Un système avec les commandes guix disponibles 
- Accès à internet depuis cette machine
- C'est tout. 

---
# Les premiers pas

Vous avez besoin d'installer un logiciel sans trop connaître le nom de sa définition GUIX ? 

`guix search nom_approximatif`

* `guix search` donne plein d'info sur les paquets : nom, version, **dépendances**

---
# Les premiers pas 

Vous voulez installer un logiciel ? 

`guix install nom_définition_GUIX`

Pour connaître les paquets que vous avez installés dans votre *profil* courant : 

`guix package -I`

Pour en supprimer :
`guix remove nom_définition_GUIX`

* Jetez un oeil à votre répertoire `$HOME/.guix-profile`

--- 
# État des définitions

Les versions des logiciels que vous installés dépendent de celles renseignées dans leurs définitions !

Pour connaître l'état des définitions (numéro de commit du channel/dépôt git dans lequel elles sont versionnées): 
`guix describe`

Pour accéder à des versions récentes des logiciels, il faut mettre à jour ces définitions (≈apt-get update) : 
`guix pull`

Pour mettre à jour les paquets installées (≈apt-get upgrade): 
`guix upgrade <nom_du_paquet>`

---
# Cas pratique

Lancement d'un script python3 utilisant numpy. 

Nous avons besoin : 
* Du script !
* D'un interpréteur python3 et de numpy
* `guix search numpy` et `guix install ...`
* *Cf. démo* : https://gricad-gitlab.univ-grenoble-alpes.fr/gricad_formations/guix-sari-2022.git

--- 
# La notion de profil

Concept similaire à **virtualenv** en python : 
* Séparation sémantique claire des env log
* Env. empilables
* Env. isolés et chargés à la demande
* Déduplication
* Reproductible : lorsque vous utilisez des manifestes déclaratifs, un profil peut être entièrement spécifié par le commit Guix qui a été utilisé pour le créer.  Cela signifie que vous pouvez recréer n’importe où et à n’importe quel moment exactement le même profil, avec juste l’information du numéro de commit.  

--- 
# Les profils en pratique (1/2)

- Le profil par défaut : 
  - le répertoire `$HOME/.guix-profile` 
- Pour créer un profil : 
`mkdir -p $HOME/.guix-extra-profiles` (qui accueillera tous nos profils)
`guix install --profile="$HOME/.guix-extra-profiles/python-stuff" python`
- Pour lister les profiles : 
`guix package --list-profile`

--- 
# Les profils en pratique 

- Pour activer un profil : 
`GUIX_PROFILE="$HOME/new_profile" ; . "$GUIX_PROFILE"/etc/profile`
- Jetez un coup d'oeil aux fichiers `etc/profile` et `$PATH`
- Ouvrez un nouveau shell pour repartir sur le profil par défaut uniquement
* On peut faire pas mal de choses (empiler les profils dans un ordre particulier, utiliser l'option `--search-paths` pour les commandes guix, etc.)
* Je ne les utilise jamais

---
# Oublions tout et utilisons `guix shell` (1/2)

`guix shell` permet de lancer, à la volée, un environnement logiciel précis, avec différents niveau d'isolation du système hôte :
* `guix shell python python-numpy`
* `guix shell --check python python-numpy`
* `guix shell python python-numpy -- python3`

* Regardez `$PATH` dans avant et dans le nouveau shell

---
# Oublions tout et utilisons `guix shell` (2/2)

* Différents niveaux d'isolation : 
  * `guix shell --pure python python-numpy`
  * `guix shell --container python python-numpy`

* Avec `guix shell`, Rien n'est installé dans votre espace utilisateur, en quittant le *shell*, il ne restera plus de trace de l'environnement (*cf* conteneurs)

---

# Et la reproductibilité, dans tout ça ? 

---
# L'environnement logiciel pour son projet

* J'ai une liste des paquets qui permettent de faire tourner mon code
* Comment faire pour que quelqu'un d'autre, sur une autre machine et à un autre moment, puisse reproduire l'environnement logiciel ? 

---
# Décrivons notre environnement logiciel une bonne fois pour toute (1/2)

Plutôt que de rentrer à la main, à chaque fois, la liste des paquets de votre environnement, **nous pouvons les lister dans une fichier texte, donc versionnable dans le dépôt de votre code source**. 

Par convention, on nomme ce fichier `manifest.scm` qui contient, pour notre exemple : 
```
(specifications->manifest
  (list "python"
        "python-numpy"))
```

---
# Décrivons notre environnement logiciel une bonne fois pour toute (2/2)

Il suffit ensuite d'indiquer ce fichier à la commande `guix shell`: 

```
guix shell -C -m manifest.scm
```

* Marche aussi avec `guix install` !
* Peut être généré avec `guix package --export-manifest <-p /chemin/vers/un/profil>`


---
# Comment être sûr de fixer la version de mes dépendances ? 

En identifiant précisément l'état de mes définitions !
* Les définitions sont dans des channels, *i.e.* des dépôts git
* Il faut donc lister les dépôts git correspondant à nos dépendances et le numéro de commit de ceux-ci sur lequel on pointe : `guix describe -f channels >> channels.scm`

* Nous avons un deuxième fichier texte, `channels.scm` à versionner au côté de notre code ! 

---
# Récapitulons

Pour décrire avec précision notre environnement logiciel, nous avons besoin de 2 fichiers dans le dépôt Git de notre code : 
- `manifest.scm` qui liste les dépendances de notre environnement logiciel
- `channels.scm`qui liste les channels utilisés et leur état

---
# Comment fais-je pour déployer cet environnement ailleurs et/ou à un autre moment ? 

- On clone le dépôt du code (code, scripts, doc, etc., `manifest.scm`, `channels.scm`)
- On lance la commande suivante : 

```
guix time-machine -C channels.scm -- shell -C -m manifest.scm
```

* `guix time-machine` donne accès à d'autres révisions de guix et lance la commande `guix` indiquée apprès `--` dans cette révision. 
* Nous déployons ici bien l'environnement logiciel indiqué dans l'état spécifié. 
* C'est tout !

---
# Comment fais-je pour déployer l'environnement logiciel sur une machine sans guix ? 

- Une machine sans les commandes guix disponibles ne pourra pas exécuter `guix time-machine`ou `guix shell`. 
- Mais, elle peut, bien souvent, exécuter des conteneurs. 
- Ça tombe bien, Guix permet de créer des images de conteneurs à partir de `manifest.scm` et `channels.scm` (sur une machine où `guix`est disponible): 

```
guix time-machine -C channels.scm -- pack --format=squashfs -m manifest.scm
```
* Ici, l'image créée est au format reconnu par singularity, on peut créer une image docker-ready ou un simple tar.gz.
* Il reste à la copier sur la machine cible sans `guix` puis à l'exécuter
* cf. démo

---
# Too Long; Did not Read (1/2)

* Sur une machine avec `guix` : 
  * On créée le `manifest.scm` contenant la liste de nos dépendances
  * `guix describe -f channels >> channels.scm` pour noter l'état des définitions de paquets qui nous conviennent. 
  * On versionne les deux fichiers à côté de notre code source

* Pour déployer sur une machine cible **avec** `guix`, on exécute sur la machine cible : 
  * `guix time-machine -C channels.scm -- shell -C -m manifest.scm`

---
# Too Long; Did not Read (2/2)

* Pour déployer sur un machine cible **sans** `guix` 
  * Sur la machine source avec `guix` : `guix time-machine -C channels.scm -- pack --format=squashfs -m manifest.scm`
  * On transfère l'image ainsi créée sur la machine cible que l'on exécute (ici avec singularity)

---
# Quelques remarques/limites (1/2)

* Nous avons passé en revue : 
  * GUIX côté "utilisateur", pas développeur de paquets
  * Il y a bien d'autres fonctionnalités/possibilités, mais j'ai présenté les outils les plus utiles et, à mon avis, le workflow le plus sain
* Si vous voulez compiler un code qui n'a pas de définition GUIX (*i.e.* à la main), il est possible d'installer les dépendances avec guix (*e.g. gcc, cmake, openmpi, fftw, etc.*) puis de le faire. Mais le résultat est *moins* garanti

---
# Quelques remarques/limites (1/2)

* Sauf cas particulier (fonctionnalités), ne vous attachez pas au numéro de versions de vos dépendances logicielles. Faites les tests avec les versions disponibles dans Guix et si ça marche, partez de la révision Guix testée. 
* N'hésitez pas à réclamer des paquets ! (en toute courtoisie, bien sûr)
* GUIX par et pour FLOSS avant tout : Certains logiciels sont réellement difficiles à empaqueter pour Guix (e.g. softs propriétaires ou avec des dépendances de logicielles propriétaires). Dans la grande majorité des cas, ces logiciels vous éloignent de la reproductibilité par design. 

---
# Pourquoi Guix plutôt qu'autre chose ? 

* Parce qu'avant de garantir la reproductibilité, c'est diablement pratique et efficace !
  * Virtualenv mais pour tous les écosystèmes
  * Le voyage dans le temps et l'espace robuste et fiable...
  * ...peu importe le système hôte
* Et c'est, avec Nix, le seul gestionnaire d'env. log. permettant, simplement, de renforcer **significativement** la reproductibilité de nos/vos travaux numériques.  
* La reproductibilité logicielle ne garantit pas la reproductibilité bit-à-bit (e.g. float comp.) d'une machine à l'autre. Mais si ce n'est pas une condition suffisante, c'est une condition nécessaire. Guix la permet, sans que ce soit compliqué.  


---

# Merci de votre attention ! 

--- 

# Miscellanées

--- 
# La documentation

* [`guix shell`](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html)
* [Les profiles avec GUIX](https://guix.gnu.org/en/cookbook/fr/guix-cookbook.fr.html#Gestion-avanc_00e9e-des-paquets)
* [`guix pack`](https://guix.gnu.org/manual/fr/html_node/Invoquer-guix-pack.html)
* [Point d'entrée de la doc](https://guix.gnu.org/en/manual/fr/guix.fr.html) et [livre de recettes](https://guix.gnu.org/en/cookbook/fr/guix-cookbook.fr.html))

---
# Et si je veux faire un paquet ? 

Vous pouvez écrire votre propre définition `mon-soft.scm` : 
```Scheme
(use-modules (guix packages)
             (guix download)
             (guix build-system gnu)
             (guix licenses))

(package
  (name "my-hello")
  (version "2.10")
  (source (origin
            (method url-fetch)
            (uri (string-append "mirror://gnu/hello/hello-" version ".tar.gz"))
            (sha256
             (base32
              "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))
  (build-system gnu-build-system)
  (synopsis "Hello, Guix world: An example custom Guix package")
  (description
   "GNU Hello long description.........")
  (home-page "https://www.gnu.org/software/hello/")
  (license gpl3+))
```

---
# Et pour l'installer ? 

* `guix package --install-from-file=mon-soft.scm`
* Explication des champs d'un paquets **en live**

---
# Cheat codes pour créer des paquets python, R, ruby, perl, etc

* `guix import pypi|cran|gem|cpan|... <--recursive> nom_du_paquet` - *Cf. démo*

---
# Pour les paquets python *compliqués*

```
guix install python
pip3 install —user torch torchvision torchaudio
```

---
# À propos des conteneurs (1/2)

2 parties dans les systèmes de conteneurs (*e.g.* docker, singularity):
* La construction de l'image : docker ou singularity sont plutôt mauvais du point de vue de la reproductibilité
* L'exécution de l'image : docker, singularity sont très bien ! 

---
# À propos des conteneurs (2/2)

Heureusement, on peut **construire des images avec guix qui sont parfaitement reproductible**!

```Sh
# Create a tar.gz file... 
$ guix pack -f docker -S /bin=bin guile guile-readline
# ...that can be directly passed to docker load...
$ docker load < file
# Then docker run
$ docker run -ti guile-guile-readline /bin/guile
```